# Theatre

## ** Please note that Theatre support has been discontinued for FVTT, this however will remain here for legacy support ** 

Theatre is a mod for FVTT as well as future unannounced VTT that allows for a visual novel style RP experience for text, and text-voice hybrid games. The primary function of Theatre is to allow for graphical 'theatre-inserts' or 'standin-graphics' to appear on screen with an accompanying area for text beneath them. This follows the style of visual novels, and even provides a means to animate or decorate the text as it appears in the below box. It also provides an emote system to allow users to configure different graphics for the various emotive expressions. Most of the emotes additionally have a built in 'emote animation' that occurs when the emote is selected, which can be toggled off globally if undesired.

## Documentation

The [wiki](https://gitlab.com/Ayanzo/theatre/wikis/home/Introduction%20to%20Theatre) on this project's git-lab will provide all needed instructions on how to use theatre.

## Installation

### For FVTT

Copy https://gitlab.com/Ayanzo/theatre/raw/master/module.json into the module installer inside foundry when it asks for the manifest.

OR

Download the zip, create a folder in public/modules called 'theatre' and extract the contents of "theatre-master.zip" there.
